
import Vue from "vue";
import Vuex from "vuex";
import getters from "./getters";
import { actions } from "./actions";
import mutations from "./mutations";

Vue.use(Vuex);

const state = {
  color : "red"
};
 
export default {
  state,
  mutations,
  actions,
  getters
};
