export const actions = {
  changeColor(context, number) {
    context.commit("CHANGE_COLOR", number);
  },
  resetColor(context) {
    context.commit("RESET_COLOR");
  }
};
