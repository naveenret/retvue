const getters = {
  getColor(state) {
    return state.color;
  }
};

export default getters;
