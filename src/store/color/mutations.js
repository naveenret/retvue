const mutations = {
  CHANGE_COLOR(state, payload) {
    state.color = payload;
  },
  RESET_COLOR(state) {
    state.color = "red";
  }
};

export default mutations;
