import Vue from 'vue'
import Vuex from 'vuex'
import VuexPersist from 'vuex-persist';
import localForage from 'localforage';
import common from './common';
import color from './color';

Vue.use(Vuex);

const vuexStorage = new VuexPersist({
    key: process.env.VUE_APP_STORAGE_KEY,
    storage: localForage,
});


export default new Vuex.Store({
    plugins: [vuexStorage.plugin],
    modules: {
        common,
        color
    },
})