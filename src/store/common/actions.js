export const actions = {
  addNumber(context, number) {
    context.commit("ADD_NUMBER", number);
  },
  removeNumber(context) {
    context.commit("REMOVE_NUMBER");
  }
};
