const mutations = {
  ADD_NUMBER(state, payload) {
    state.numbers.push(payload);
  },
  REMOVE_NUMBER(state) {
    state.numbers.pop();
  }
};

export default mutations;
