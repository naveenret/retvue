const getters = {
  getNumbers(state) {
    return state.numbers;
  }
};

export default getters;
