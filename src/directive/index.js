import Vue from "vue";

export const red = {
  bind(el) {
    el.style.color =  "red";
  }
};

export const blue = {
  bind(el, binding) {
    el.style.color = "blue";
    console.log("bindings", binding);
    console.log("bindings", binding.value);
    const modifiers = binding.modifiers;
    if (modifiers.underline) {
      el.style.textDecoration = "underline";
    }
  }
};

export const hover = {
  bind(el, binding) {
    el.style.color = "blue";
    console.log("bindings", binding);
    console.log("bindings", binding.value);
    const modifiers = binding.modifiers;
    if (modifiers.underline) {
      el.style.textDecoration = "underline";
    }
  },
  inserted(el) {
    // inserted(el, bindings, vnode) {
    el.style.position = "relative";
    el.style.color = "brown";
    // var node = document.createElement("li");
    // node.className = "hover-node1";
    // node.className = node.className.concat(" hover-node-1");
    // // node.style.display = "none";
    // node.style.position = "absolute";
    // node.style.width = "100%";
    // node.style.color = "red";
    // node.style.top = "-50px";
    // var textnode = document.createTextNode("Water");
    // node.appendChild(textnode);
    // el.appendChild(node);

    // console.clear();
    // console.log('inserted')
    // el.addEventListener('mouseenter', function () {
    //   el.style.border = "1px solid black";
    //   el.style.color = "green";
    //   el.style.backgroundColor = "white";
    // });
    // el.addEventListener('mouseenter', function () {
    //   el.style.border = "1px solid black";
    //   el.style.color = "green";
    //   el.style.backgroundColor = "white";
    // });
  }
};

export const orange = {
  bind(el) {
    el.style.color = "orange";
  },
  inserted(el, bindings, vnode) {
    console.log("Inserted el called", el);
    console.log("Inserted bindings called", bindings);
    console.log("Inserted vnode called", vnode);
  }
};

Vue.directive("red", red);
Vue.directive("blue", blue);
Vue.directive("orange", orange);
Vue.directive("hover", hover);
// Vue.directive("hover", el => {
//   el.style.position = "relative";
//   el.style.color = "brown";
//   var node = document.createElement("li");
//   node.className = "hover-node1";
//   node.className = node.className.concat(" hover-node-1");
//   // node.style.display = "none";
//   node.style.position = "absolute";
//   node.style.width = "100%";
//   node.style.color = "red";
//   node.style.top = el.clientHeight;
//   console.clear();
//   console.log(el.clientHeight);
//   var textnode = document.createTextNode("Water");
//   node.appendChild(textnode);
//   el.appendChild(node);
// });
