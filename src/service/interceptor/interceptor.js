import Vue from "vue";
import axios from 'axios';
// import store from 'your/store/path/store'

export default function setup() {
    axios.interceptors.request.use(
        response => successHandler(response),
        error => Promise.reject(error)
    )
}

const errorHandler = (error) => {
    // if (isHandlerEnabled(error.config)) {
    //     // Handle errors
    // }
    console.log('interceptor error ', error);
    console.log('interceptor error ', error.config);
    console.log('interceptor error ', error.response);
    return Promise.reject({ ...error })
}


const successHandler = (response) => {
    console.log('interceptor response', response);
    // if (isHandlerEnabled(response.config)) {
    //     // Handle responses
    // }
    return response
}


const errorSetup = function setup() {
    axios.interceptors.response.use(
        null,
        error => errorHandler(error)
    )
}

export { errorSetup }