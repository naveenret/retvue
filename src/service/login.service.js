import ApiService from "./api.service";

export const LoginService = {
  login(data) {
    return ApiService.post(`/ananya/api/login/auth`, data);
  }
};
