import home from "../home";
import { LoginService } from "../../service/login.service";

export default {
  beforeCreate() {
    console.log("beforeCreate");
  },
  created() {
    console.log("Created");
  },
  beforeMount() {
    console.log("beforeMount");
  },
  mounted() {
    console.log("mounted");
  },
  beforeUpdate() {
    console.log("beforeUpdate");
  },
  updated() {
    console.log("updated");
  },
  beforeDestroy() {
    console.log("beforeDestroy");
  },
  destroyed() {
    console.log("destroyed");
  },
  name: "login",
  components: {
    home
  },
  props: [],
  data() {
    return {
      formLayout: "horizontal",
      form: this.$form.createForm(this)
    };
  },
  methods: {
    handleSubmit(e) {
      e.preventDefault();
      this.form.validateFields((err, values) => {
        if (!err) {
          console.log("Received values of form: ", values);
          let data = {
            userId: values.Username,
            password: values.Password
          };
          LoginService.login(data)
            .then(res => {
              console.log("res", res);
              this.$router.push("home");
            })
            .catch(err => {
              console.log("err", err);
              console.log("err", err.data);
            });
        }
      });
    }
  }
};
