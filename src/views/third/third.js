export default {
  name: "third",
  components: {},
  props: [],
  data() {
    return {};
  },
  computed: {
    getNumbers() {
      console.log("this.$store.getters", this.$store.getters);
      return this.$store.getters.getNumbers;
    }
  },
  mounted() {},
  methods: {
    removeNumber() {
      this.$store.dispatch("removeNumber");
    }
  }
};
