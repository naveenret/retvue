export default {
  name: 'typeahead',
  components: {},
  props: {
    inputType: {
      default: "number",
    },
    dropArray: {
      default: []
    },
    defaultValue: {
      default: ''
    }
  },
  data() {
    return {
      items: this.dropArray,
      inputValue: this.defaultValue
    }
  },
  computed: {

  },
  mounted() {
    console.log("mounted", this);
    console.log("input type", this.inputType);
    console.log("input type", this.$refs);
    console.log("input type", this.$el);
    this.$el.addEventListener('click', this.stopProp);
    if (this.$refs.typeAheadInputDropdownArea) {
      document.body.addEventListener('click', this.onClickOutside, true)
    }
  },
  methods: {
    stopProp(event) { event.stopPropagation() },
    inputEmit(e) {
      console.log('input emit in typeahead', e);
      console.log("input type", this.$refs.typeAheadInputDropdownArea);
      this.$emit('inputEmit', e);
      this.$refs.typeAheadInputDropdownArea != undefined ? this.$refs.typeAheadInputDropdownArea.style.display = 'block' : null;
    }, 
    onClickOutside() {
      if (this.$refs.typeAheadInputDropdownArea && this.$refs.typeAheadInputDropdownArea.style.display != 'none') {
        console.log('blur', this.$refs);
        this.$refs.typeAheadInputDropdownArea != undefined ? this.$refs.typeAheadInputDropdownArea.style.display = 'none' : null;
      }
    },
    valueSelected(i) {
      console.log('type ahead', i);
      console.log(this.$refs.typeAheadInput);
      console.log(this.items[i]);
      console.log(this.$refs.typeAheadInputDropdownArea);
      this.$emit("nvChange", this.items[i]);
      this.inputValue = this.items[i];
      this.$refs.typeAheadInputDropdownArea != undefined ? this.$refs.typeAheadInputDropdownArea.style.display = 'none' : null;
    },

  }
}
