import nvimagethumb from '../nv-upload/nv-image-thumb';
export default {
  name: 'nv-upload',
  components: {
    nvimagethumb
  },
  props: {
    round: Boolean,
    accept: {
      default: null
    },
    fileList: {
      default: Array
    },
    length: {
      default: 1
    },
    image: Boolean,
    video: Boolean,
  },
  data() {
    return {
      images: parseInt(this.length) ?
        (
          this.fileList.length <= parseInt(this.length) ?
            (
              this.fileList || []
            ) :
            (
              this.fileList.splice((length - 1), (this.fileList.length - length))
            )
        ) :
        (this.fileList
          ?
          this.fileList :
          []
        ),
      isRound: this.round,
      isVideo: this.video,
      isImage: this.image,
      isAccept: (this.image || this.video) ?
        (
          this.image ?
            (
              ".jpg,.jpeg,.png"
            ) :
            (
              this.video ?
                (".avi,.3gp,.ogg,.mp4,.wmv,.flv,.webm,.wmv") :
                (this.accept)
            )
        ) :
        (
          this.accept
        ),
      isLength: parseInt(this.length) || null
    }
  },
  computed: {

  },
  mounted() {
    console.log('mounted',this.video, this.image)
  },
  methods: {
    uploadChange(e) {
      var files = e.target.files || [];
      if (files) {
        Array.prototype.forEach.call(files, (item) => {
          if (this.isLength >= files.length + this.images.length) {
            var reader = new FileReader();
            console.log('files',files);
            if(files && files[0]  ){
            if(
              files[0].type == 'video/mp4' ||
              files[0].type == 'video/3gp' ||
              files[0].type == 'video/ogg' ||
              files[0].type == 'video/wmv' ||
              files[0].type == 'video/webm' ||
              files[0].type == 'video/flv' ||
              files[0].type == 'video/avi' ||
              files[0].type == 'video/3gp2' 
            ){
              this.isVideo = true;
            }else{
              this.isVideo = false;
              }
            }
            reader.addEventListener("load", function () {
              this.images.push(reader.result);
              this.$emit('nvUploadChange', files);
            }.bind(this), false);
            reader.addEventListener("error", function () {
              console.log("error")
            }.bind(this), false);
            reader.readAsDataURL(item)
          }
        });
      }
    },
  }
}
