export default {
  name: 'nv-image-thumb',
  components: {},
  props: {
    items: {
      default: [],
    },
    round: Boolean,
    length: {
      default: 1
    },
    image: Boolean,
    video: Boolean,
  },
  data() {
    return {
      images: this.items || [],
      isRound: this.round ? { 'border-radius': '50%' } : null,
      previewImage: null,
      showPreview: false,
      initialScrollPosition: 0,
      isLength: parseInt(this.length)
    }
  },
  watch: {

    items() {
      console.log('watch called', this.items);
      if (this.video && this.items[0]) {
        // document.querySelector("#nv-upload-video-element source").setAttribute('src', URL.createObjectURL(this.items[0]));
      }
    }
  },
  computed: {
    showUpload() {
      console.log('this.isleng', this.isLength);
      if (!isNaN(this.isLength) && this.isLength != 0) {
        return this.isLength > this.images.length;
      } else {
        console.log('this.imagek,z', this.images);
        console.log('this.imagek,z', this.images.length);
        return true;
      }
    }
  },
  mounted() {
    console.log('mounted', this.video, this.image, this.images[0]);


  },
  methods: {
    noScroll() {
      window.scrollTo(0, 0);
    },
    deleteAction(e) {
      console.log('delete action', e);
      this.images.splice(e, 1);
      this.$emit('nvUploadThumbDelete', e);
    },
    closePreview() {
      this.showPreview = false;
      window.scrollTo({
        top: this.initialScrollPosition,
        left: 0,
      });
      window.removeEventListener('scroll', this.noScroll);
    },
    previewAction(e) {
      console.log('preview action', e);
      new Promise((resolve) => {
        this.previewImage = this.images[e] || null;
        resolve();
      }).then(() => {
        this.initialScrollPosition = window.scrollY;
        if (!this.showPreview) {
          window.scrollTo({
            top: 0,
            left: 0,
          })
          this.showPreview = true;
        }
      }).then(() => {
        window.addEventListener('scroll', this.noScroll);
      });


    }
  }
}
