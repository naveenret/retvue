export default {
  name: 'nv-button',
  components: {},
  props: [],
  data () {
    return {

    }
  },
  computed: {

  },
  mounted () {

  },
  methods: {
    submit(){
      console.log('nv button submit called');
      this.$emit('nvSubmit');
    }
  }
}
